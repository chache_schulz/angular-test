(function () {
    'use strict';
    angular
        .module('angularApp')
        .service('weatherApiService', weatherApiService);

    weatherApiService.$inject = ['$http'];

    function weatherApiService($http) {
        var host = 'http://api.openweathermap.org/data/2.5/forecast/daily?units=metric&';
        var apiKey = '157500dfc223657bf6699ccad2bef9fe';

        var service = {
            getWeather: getWeather
        };
        return service;

        function getWeather(city, country, count) {
            var q = city + ',' + country;
            return $http.get(host + 'q=' + q + '&cnt=' + count + '&appid=' + apiKey);
        }
    }
})();