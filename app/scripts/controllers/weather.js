'use strict';

/**
 * @ngdoc function
 * @name angularApp.controller:WeatherCtrl
 * @description
 * # WeatherCtrl
 * Controller of the angularApp
 */
angular.module('angularApp')
  .controller('WeatherCtrl', function ($scope, $http, weatherApiService) {
    $scope.daysOptions = Array.apply(null, Array(15)).map(function (_, i) {return i+1;});
    $scope.daysSelected = 7;

    $scope.updateWeather = function()
    {
      var scope = this;
      weatherApiService.getWeather('LaPlata','AR',scope.daysSelected).
      then(function(response) {
          scope.weatherData = response.data;
          scope.lastUpdated = Date.now();
      });
    };

    $scope.updateWeather();
  });
